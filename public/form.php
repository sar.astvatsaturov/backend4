<!DOCTYPE html>
<html>
	<head>
        <link rel="shortcut icon" href="logo.png" type="image/png">
 		<title>backend4</title>
 		<style>
            body {
                background-color: white;
                font-weight: 700;
                font-size: x-large;
            }
            form {
                background-color: white;
                border-radius: 30px;
                text-align: center;
                padding: 100px;
                width: 50%;
                height: 90%;
                margin: auto;
            }
            label {
                font-size: medium;
            }
            input, select, textarea {
                margin-bottom: 20px;
            }
         .error {
            border: 2px solid red;
        }
        .error-radio
        {
            border: 2px solid red;
            width: 150px;
        }
        .error-bio
        {
            border: 2px solid red;
            width: 250px;
            height: 16px;
        }
        .error-check
        {
            border: 2px solid red;
            width: 350px;
        }
        .error-abil
        {
            border: 2px solid red;
            width: 290px;
            height: 70px;
        }
        .error-radio-limb
        {
            border: 2px solid red;
            width: 250px;
            height:35px;
        }
    </style>
</head>
<body>
<form action="" method="POST">
    <label>
      Имя:<br />
      <input name="fio" type="text" <?php if ($errors['fio']) print('class="error"');?>  value="<?php print($NAME);?>"  />
    </label><br />
    <label>
      Email:<br />
      <input name="email" <?php if ($errors['email']) print('class="error"');?> 
        value="<?php print($EMAIL);?>"
        type="email" />
    </label><br />
    <label>
      Год рождения:<br />
    <select name="year" >
      <?php for ($i = 1900; $i < 2021;$i++) {
        print('<option value="');
        print($i);
        if ($YEAR == $i) print('" selected="selected">');
	else print('">');
        print($i);
        print('</option>');}?>
    </select>
    </label><br />
    Пол:<br />
    <label><input type="radio" <?php if ($SEX == "М") print("checked"); ?>
      name="sex" value="М" />
      М</label>
    <label><input type="radio" <?php if ($SEX == "Ж") print("checked");  ?> 
      name="sex" value="Ж" />
      Ж</label><br />
    Количество конечностей:<br />
<?php for ($i = 1; $i <= 4; $i++) {
    print('<label><input type="radio" name="limbs"');
    if ($LIMB == $i) print('checked="checked" value="');
    else print('value="'); 
    print($i);
    print('"/>');
    print($i);
    print('</label>');
}
?>
<br />
    <label>
        Способности:
        <br />
        <select name="power[]"
          multiple="multiple">
          <option value="ab_god" <?php if ($GOD) print('selected');  ?> >Бессмертие</option>
          <option value="ab_clip" <?php if ($CLIP) print('selected');  ?> >Прохождение сквозь стены</option>
          <option value="ab_fly" <?php if ($FLY) print('selected');  ?>>Левитация</option>
        </select>
    </label><br />
    <label>
      Биография:<br />
      <textarea name="bio"><?php print($BIO);?> </textarea>
    </label><br />
    <label><input type="checkbox"
      name="check" required/>
      С контрактом ознакомлен</label><br />
    <input type="submit" value="Отправить" />
</form>
</body>
</html>
